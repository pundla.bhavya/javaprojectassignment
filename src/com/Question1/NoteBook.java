package com.Question1;

public class NoteBook extends Book{
	
	@Override
    public void write() {
        System.out.println("Writing in the notebook...");
    }

    @Override
    public void read() {
        System.out.println("Reading from the notebook...");
    }

    public void draw() {
        System.out.println("Drawing in the notebook...");
    }
public static void main(String[] args) {
	NoteBook n=new NoteBook();
	n.write();
	n.read();
	n.draw();
}

}
