package com.Question2A;

public interface SmartTVremote extends TVremote {
	void mute();
	void volumeUp();
}
