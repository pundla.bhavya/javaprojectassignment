package com.Question2A;

public class TV implements SmartTVremote {
	  @Override
	  public void on() {
	    System.out.println("Turning on TV");
	  }
	  @Override
	  public void off() {
	    System.out.println("Turning off TV");
	  }
	  @Override
	  public void mute() {
	    System.out.println("Muting TV");
	  }
	  @Override
	  public void volumeUp() {
	    System.out.println("Turning up TV volume");
	  }
}
