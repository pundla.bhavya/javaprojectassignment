package com.Question2A;

public class SmartTelephone extends Telephone{

	@Override
	public void lift() {
		System.out.println("Lifting a smart telephone");
	}
	@Override
	public void disconnected() {
		System.out.println("Disconnecting a smart telephone");
	}
}