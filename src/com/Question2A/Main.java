package com.Question2A;

public class Main {

	public static void main(String[] args) {
		Telephone telephone = new SmartTelephone();
		telephone.lift();
		telephone.disconnected();
		TV tv = new TV();
		tv.on();
		tv.off();
		tv.mute();
		tv.volumeUp();
		
		}
}
