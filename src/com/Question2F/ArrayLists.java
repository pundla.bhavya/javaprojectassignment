package com.Question2F;

import java.util.ArrayList;


public class ArrayLists {

	public static void main(String[] args) {
		ArrayList<String> list = new ArrayList<>();
		
		list.add("thumbsup");
		list.add("mountain");
		list.add("pulpi");
		list.add("coke");
		list.add("sprite");
		
		
		System.out.println("the size of arraylist: " +list.size());
		System.out.println(list);
		
		String removestring=list.remove(2);
		System.out.println("Resulting ArrayList: "+list);
		
		System.out.println(list);
		System.out.println(removestring);
		
	}

}
