package com.Quetion2C;

public class Hello_World {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		  String str = "Hello, World";
	       
	        int firstIndexOfO = str.indexOf('o');
	        int lastIndexOfO = str.lastIndexOf('o');
	        int firstIndexOfComma = str.indexOf(',');
	        int lastIndexOfComma = str.lastIndexOf(',');
	       
	        System.out.println("First occurrence of 'o': " + firstIndexOfO);
	        System.out.println("Last occurrence of 'o': " + lastIndexOfO);
	        System.out.println("First occurrence of ',': " + firstIndexOfComma);
	        System.out.println("Last occurrence of ',': " + lastIndexOfComma);
	      }

}
