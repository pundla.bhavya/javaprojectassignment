package com.Question2E;

class Area extends Shape {
    @Override
    void RectangleArea(double length, double breadth) {
        System.out.println("Area of rectangle: " + (length * breadth));
    }

    @Override
    void SquareArea(double side) {
        System.out.println("Area of square: " + (side * side));
    }

    @Override
    void CircleArea(double radius) {
        System.out.println("Area of circle: " + (Math.PI * radius * radius));
    }
}
